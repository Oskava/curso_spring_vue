const PI = 3.1415926;

let unaVar = 20;
let unTexto = "Que Pasa!";

document.write(`<br>
Texto en varias
líneas, y además, podemos mostrar <br>
variables así: ${unaVar} y otro texto: ${unTexto}`);

document.write("<br>");

//Funciones Lambda: funciones anónimas o funciones flecha
//para JavaScript
var suma = (x, y) => x + y

document.write("<br>" + suma(3, 2));

var alcuadrado = x => x ** 2;
document.write("<br>" + alcuadrado(3));

class Dato {
    //sobre dato
    constructor(xxx, yyy = 20) {
        this.x = xxx;
        this.y = yyy;
    }
    mostrar() {
        document.write(`<br> Dato: x = ${this.x} y = ${this.y}`);
    }
}
class Info extends Dato {
    constructor(xx, yy = 20, zz = 20) {
        super(xx, yy);
        this.z = z;
    }
    mostrar() {
        super.mostrar();
        document.write(` z=${this.z}`);
    }
}
//llamamos al constructor Dato, genera un objeto y al darle new Dato se añade el valor al campo que no tenga ningun valor
let dato = new Dato("lo que quieras")
dato.mostrar();
//Aqui creamos un nuevo objeto de tipo info, cuya x vale lo que tu quieras
let info = new Info("Otra info")
info.mostrar();