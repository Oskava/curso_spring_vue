// JSON: JavaScript Object Notation
//Otra forma de crear objetos con la notación JSON
let objetoVacio = {}; //Es lo mismo que new Object()

/*let formaPago = {
    "modo": "Tarjeta credito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes": ["Santander", "Sabadell", "BBVA", [1, 23, 55]], //Array
    "configuracion": {
        "conexion": "ssl",
        "latencia": 15
    }
};*/
//Leer de disco el JSON y DES-SERIALIZARLO
let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"));
let arrayVacio = []; //new Array()
let datos = ["Churros", "Meninas", 200, true, null, { "ale": "mas datos" }];
let matriz = [
    [4, 6, 8],
    [3, 7, 7],
    [1, 5, 7]
];
formaPago.servidor = "http://visa.com";
formaPago["oculta"] = "Dame 5 centimos para mi";
//stringify convierte un objeto o estructura en memoria de JavaScript en un formato transmitible (o para enviar por red o para guardar en ficheros),
//es SERIALIZAR, y el formato puede ser texto (XML, JSON, YAML, o un propio), formato binario, encriptado
document.write(`<br>
<p>${formaPago.modo} - ${formaPago.clientes[1]} - ${formaPago.clientes[3][2]}</p> ${matriz[2][1]} 
<code>${JSON.stringify(formaPago,null,3)}</code>
Usando forma de HashMap: ${formaPago["servidor"]}
`);
//el 3 son los espacios que se genera
alert(JSON.stringify(formaPago, null, 3));
//Guardar en el disco JSON en el espacio local de la pagina( unos 20 MB)
//Usando esto se podría guardar los datos para su uso sin conexión
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));
let petUsu = prompt("¿Qué dato quieres?");
document.write(`<br> ${formaPago[petUsu]}`);
let frutas = `[
    {   "nombre": "pera", "precio": 20  },
    {   "nombre": "kiwi", "precio": 27  },
    {   "nombre": "fresa", "precio": 37 }]`;
//Parsear es la forma coloquial de decir "leer o interpretar un texto"
//puede ser convertir un texto en otro texto, o en este caso, 
//convertir un texto en un objeto en estructura en memoria. Cuando hablamos de convertir
//un texto en un objeto, la definición técnica es "DES-SERIALIZAR"
let objFrutas = JSON.parse(frutas);
document.write(`<br> ${objFrutas[1].nombre} - ${objFrutas[2]["precio"]}`);