package com.example.prueba;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

//HErencia de la clase AppComparActivity
public class MainActivity extends AppCompatActivity {

    //Hace referencia al objeto de creación de la aplicación, es decir, lo primero que se ejecuta
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //indica cual va a ser la primera vista inicial que se va a mostrar. R viene de Resource.
        setContentView(R.layout.activity_main);
        //findView busca un elemeno por ID, en este caso el menu toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Buscamos el boton  flotante
        FloatingActionButton fab = findViewById(R.id.fab);
        //Recepcionador del evento de pulsado para que haga lo indicado. El objeto en este caso, como no tiene nombre, se denomina como anonimo
        //al abrir llaves dentro del objeto, creamos una clase interna
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            //Cuando se clicka (pulsa) el botón
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    //Cuando se crea el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Inflado equivale a pintarlo en la pantalla
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Cuando selecciona la opcion del menu y se saca el id del item seleccionado
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}