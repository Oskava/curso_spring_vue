package com.grupoica.ejem05modelo;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        DerbyDBUsuario m= new DerbyDBUsuario();
        //ServicioUsuarios.getInstancia().addUsuario("Oscar ", 26, "oscarcabaquero@gil.com", "11021994");
        //ServicioUsuarios.getInstancia().addUsuario("Sandra", 27, "sasndra@gil.com", "afg565");

        ArrayList<Usuario> usuarios = m.listar();

        for(Usuario usuario:usuarios) {
        	System.out.println("Usu: " + usuario.getNombre() + " con edad " + usuario.getEdad());
        }
        
        //Muestra el número de Usuarios que hay en la tabla
        System.out.println("El numero de usuarios es :" + ServicioUsuarios.getInstancia().cantidadUsuarios());
    }
}

