package com.grupoica.repasojava.abstractas_interfaces;

public class Perro  implements Animal{

	String ladrido;
	
	public Perro(String lad) {
		ladrido=lad;
	}
	@Override
	public void alimentarse(String comida) {
		if(comida.contains("Chuleton")){
			System.out.println("Come Carne " + comida);
		} else {
			System.out.println(ladrido + " quiero Chuleton no quiero " + comida);
		}
	}
}
