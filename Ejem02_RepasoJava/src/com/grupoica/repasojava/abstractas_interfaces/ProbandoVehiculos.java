package com.grupoica.repasojava.abstractas_interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProbandoVehiculos {

	public static void probar() {
		Coche miCoche=new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		
		Coche miCocheFines=new Coche("Hammer", 15000, 80.34f);
		miCocheFines.aceleracion();
		
		Caballo miCaballo=new Caballo("Potro",450,25);
		miCaballo.aceleracion();
				
		//Poliformismos: se pasa la referencia
		//(el puntero, la direcci�n de memoria)
		Vehiculo unVehiculo=miCoche; //Casting Implicito
		Object unObjeto=miCoche;
		Coche unCoche=(Coche) unObjeto; // Casting Explicito
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();
		ArrayList<Motorizable> garaje= new ArrayList<>();
		//garaje.add(miCaballo); //Caballo no es motorizable
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		//No se puede porque la clase es Abstracta
		//garaje.add(new Vehiculo("Que no he comprado",30));
		garaje.add((Coche)unVehiculo);
		garaje.add(new Patinete(15));

		for(Motorizable objMotor : garaje) {
			objMotor.encender();
			if(objMotor instanceof Vehiculo) {
				Vehiculo vehiculo=(Vehiculo) objMotor;		
				vehiculo.aceleracion();
				vehiculo.desplazarse(1.5f);
			}		
		}	
		System.out.println("");
		miCoche.encender();
		Motorizable vehMotor = miCoche; 
		vehMotor.encender();
		
		HashMap<String, Animal> granja = new HashMap<>();
		granja.put("perro", new Perro("Guau guay"));
		granja.put("caballo", new Caballo ("Unicornio", 500, 20));
		granja.put("caballo2", miCaballo);
		for(Map.Entry<String, Animal> animal : granja.entrySet()) {
			animal.getValue().alimentarse("Pollo");
			animal.getValue().alimentarse("Calabacin");
		}

	}
}
