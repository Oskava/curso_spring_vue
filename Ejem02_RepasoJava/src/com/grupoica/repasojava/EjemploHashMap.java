package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;


import com.grupoica.repasojava.modelo.Usuario;;

//Un Hashmap es una coleccion de elementos del mismo tipo, din�mica pero que en
//vez de acceder a sus elementos por un indice, se accede por el tipo de variable que se haya creado

public class EjemploHashMap {
	public class ClaveDicc{
		String nombre;
		String fecha;
	}
	
	//public static HashMap<ClaveDicc, Usuario> diccUsuarios = new HashMap<>();
	public static HashMap<String, Usuario> diccUsuarios = new HashMap<>();

	public static void probandoHashMap() {
		diccUsuarios.put("Luis", new Usuario("Luis",18));
		diccUsuarios.put("Ana", new Usuario("Ana",20));
		diccUsuarios.put("Luisa", new Usuario("Luisa",30));
		diccUsuarios.put("U2",new Usuario("U2",999));
		Scanner escaner= new Scanner(System.in);
		System.out.println("Introducza el usuario");
		String nombre = escaner.nextLine();
		System.out.println("El usuario es " 
				+ diccUsuarios.get(nombre).toString());
	}
}
