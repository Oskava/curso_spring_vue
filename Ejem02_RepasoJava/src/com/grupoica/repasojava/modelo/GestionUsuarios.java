package com.grupoica.repasojava.modelo;

import java.util.ArrayList;

import com.grupoica.repasojava.EjemploHashMap;

/*Clase que se encargar� de la operaciones C.R.U.D.
 * Create Read Update y Delete (Operaciones de Alta, baja, modificaci�n y consulta)
 */
public class GestionUsuarios {


	// lista en su forma ambigua ( todos los elementos son object)
	// private ArrayList listaUsuarios; // es lo mismo que ArrayList<Object>

	// lista en su forma gen�rcia ( todos los elementos son del mismo tipo o alg�n
	// tipo heredado)
	private ArrayList<Usuario> listaUsuarios;

	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList();
		// this.listaUsuarios.add(10);
	}

	public void listarUsuarios() {
		for (int i = 0; i < listaUsuarios.size(); i++) {
			System.out.println(listaUsuarios.get(i));
		}
	}

	// Declaraci�n e implementaci�n de la funci�n listarUsuarios
	public void listarUsuario(String nombre) {

		for (Usuario usu : listaUsuarios) {
			// if(usuObj instanceof Usuario) {
			// Casting (conversi�n de un tipo de dato a otro, gracias al polimorfismo)
			// Usuario usu = (Usuario) usuObj;
			if (usu.getNombre().equals(nombre)) {
				System.out.println("ENCONTRADO: " + usu.getNombre());
			}
			// }
		}
	}

	public Usuario obtenerUsuario(String nombre) {

		for (Usuario usu : listaUsuarios) {
			// El ignore case le da igual si hay mayusculas y minusculas
			if (usu.getNombre().equalsIgnoreCase(nombre)) {
				return usu;
			}
		}
		return null;
	}

	public void eliminarUsuario(String nombre) {

		for (Usuario usu : listaUsuarios) {
			// El ignore case le da igual si hay mayusculas y minusculas
			if (usu.getNombre().equalsIgnoreCase(nombre)) {
				System.out.println("Usuario Eliminado: " + nombre);
				listaUsuarios.remove(usu);
				return;
			}
		}
	}

	// Lo que se a�ade aqui es una referencia
	public void a�adirUsuario(Usuario usu) {
		this.listaUsuarios.add(usu);
		//Para a�adirlo tambien al HashMap
		EjemploHashMap.diccUsuarios.put(usu.getNombre(),usu);	


	}
	
	public void a�adirUsuario(String nombre, int edad) {
		Usuario nuevoUsu=new Usuario(nombre,edad);
		this.listaUsuarios.add(nuevoUsu);
		EjemploHashMap.diccUsuarios.put(nuevoUsu.getNombre(), nuevoUsu);
	}

	public void modificar(Usuario usu, String nombre, int edad) {
		this.listaUsuarios.remove(usu);
		this.listaUsuarios.add(usu);
		usu.setEdad(edad);
		usu.setNombre(nombre);
		System.out.println(("Usuario " + usu.getNombre() + "[" + usu.getEdad() + "]"));

	}

	public void modificar(Usuario usu, String nombre) {
		this.listaUsuarios.remove(usu);
		this.listaUsuarios.add(usu);
		usu.setNombre(nombre);
		System.out.println(("Usuario " + usu.getNombre()));
	}
	
	public void modificar(String nombre, String nuevoNombre, int nuevaEdad) {
		Usuario usuBorrar= obtenerUsuario(nombre);
		if(usuBorrar !=null) {
			usuBorrar.setEdad(nuevaEdad);
			usuBorrar.setNombre(nuevoNombre);
		}
	}
	
	public void modificar(String nombre, String nuevoNombre) {
		Usuario usuBorrar= obtenerUsuario(nombre);
		if(usuBorrar !=null) {
			usuBorrar.setNombre(nuevoNombre);
		}
	}
	
	public void modificar(String nombre, int edad) {
		Usuario usuBorrar= obtenerUsuario(nombre);
		usuBorrar.setEdad(edad);
		System.out.println(("Usuario " + nombre+ usuBorrar.getEdad()));
	}

	public void filtrarXEdad(int edadMin, int edadMax) {

		for (Usuario usu : listaUsuarios) {
			if (usu.getEdad() >= edadMin && usu.getEdad() <= edadMax) {
				System.out.println("ENCONTRADO: " + usu.toString());
			} else {
				System.out.println("Usuario que no coincide: " + usu.toString());
			}
		}
	}
	
	public void filtrarXEdad(int edad) {
		filtrarXEdad(edad, edad);		
		}
	
	public void eliminarTodos() {
		listaUsuarios.clear();
	}
}
