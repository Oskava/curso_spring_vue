package com.grupoica.repasojava.modelo;

//T�pica clase Plain Old Java Object POJO, es decir,
//es una clase con funcionalidad simple, pero destaca 
//que no usa clases externas 
public class Usuario extends Object {

	private int edad;
	private String nombre;
	
	public Usuario() {
		super();
		nombre= "Sin nombre";
	}
	
	//Sobrecarga de constructores
	public Usuario(String nombre, int edad) {
		this.nombre=nombre;
		this.edad=edad;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
// Con arroba empiezan las anotaciones, que son extras al c�digo
	@Override
	public boolean equals(Object obj) {
		Usuario usuario = (Usuario) obj;
		return this.nombre==usuario.nombre 
				&& this.edad == usuario.edad;
	}
	
	//@Override no se puede poner Override porque es una sobrecarga
	//2 metodos o construcores con el mismo nombre pero con distinta
	//signatura (es decir, los par�metros son distintos)
	public boolean equals(Usuario usuario) {
		return this.nombre==usuario.nombre 
				&& this.edad == usuario.edad;
	}
	// Override significa sobreescribir: Machacar el metodo del padre

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario " + nombre + " [" + edad + "]";
	}
	
	
}
