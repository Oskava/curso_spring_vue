package com.grupoica.repasojava;

import com.grupoica.repasojava.abstractas_interfaces.ProbandoVehiculos;
import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.Usuario;

public class ProgramaMain {

	//Esto es una constante, es decir que no es modificable
	public static final float PI=3.1415927f;
	/*
	 * P.O.O La unidad b�sica de almacenamiento son los tipos primitivos y los
	 * objetos que est�n basados en clases, Las clases son el
	 * molde/plantilla/estructura que indica como ser�n todos los objetos
	 * instanciados a partir de ella: en esencia, sus variables miembro (campos,
	 * atributos, propiedades...) y sus m�todos (funciones propias) - Encapsualci�n:
	 * Capacidad de las clases para limitar a variables miembro y m�todos (nivel de
	 * acceso private (a nivel de clase), public, protected (a nivel de herencia) o
	 * por defecto (a nivel de paquete) - Herencia: La capacidad de las clases para
	 * heredar los m�todos y variables miembro unas de otras. Usando la palabra
	 * extends. - Polimorfismo: La capacidad de los objetos de obtener su forma de
	 * su clase o
	 */// la de cualquiera de sus ancestros (padre, abuelo...)

	public static void main(String[] args) {
		EjemploLambdas.ejecutarLambdas();
		//ProbandoVehiculos.probar();
		//return;
		/*
		GestionUsuarios gesUsu = new GestionUsuarios();
		// gesUsu.a�adirUsuario("HOLA");
		gesUsu.listarUsuarios();

		// EjemploMemoria.pruebaPasoPorValor();
		// EjemploMemoria.pruebaPasoPorReferencia();

		double doble = 10.43434;
		int entero = (int) doble; // Casting (conversion de un tipo de dato en otro, se perderian los decimales)
		// int entero = (int) Math.floor(doble); otro tipo de cast

		Usuario usu = new Usuario();
		usu.setEdad(30);
		usu.setNombre("Fulanito Mengano");
		System.out.println("Nombre: " + usu.getNombre());
		System.out.println("Edad: " + usu.getEdad());
		System.out.println("Edad de " + usu.getNombre() + ": " + usu.getEdad());

		// Si lista usuarios es un ArrayList sin tipo, es que es su tipo es Object, cada
		// uno
		// de los elementos es object. Por lo tanto hacemos un casting impl�cito (sin
		// parentesis) de usu a object
		// gracias al polimorfismo
		gesUsu.a�adirUsuario(new Usuario("Pepe", 20));
		gesUsu.listarUsuarios();

		Usuario u2 = new Usuario("U2", 50);
		System.out.println("Edad u2: " + u2.getEdad());
		gesUsu.a�adirUsuario(new Usuario("Oscar", 20));

		Usuario u3 = new Usuario("User3", 10);
		System.out.println("Edad de " + u3.getNombre() + ": " + u3.getEdad());
		gesUsu.a�adirUsuario(new Usuario("German", 30));

		// this.listaUsuarios.add("Texto");
		// this.listaUsuarios.add(new Object());
		gesUsu.listarUsuarios();

		if (u2.equals(usu)) {
			System.out.println("Son Iguales");
		} else {
			System.out.println("Son Diferentes");
		}
		Loco joker = new Loco();
		joker.setNombre("Joker");
		joker.setTipoLocura(true);
		if (joker.isTipoLocura()) {
			System.out.println("Joker esta loco: " + joker.toString());
		} else {
			System.out.println("Joker no esta loco: " + joker.toString());
		}
		System.out.println("Joker: " + joker.getNombre());
		joker.setEdad(30);
		gesUsu.a�adirUsuario(u2);
		gesUsu.a�adirUsuario(joker);
		gesUsu.a�adirUsuario("Joker", 100);
		gesUsu.listarUsuario("Joker");
		gesUsu.eliminarUsuario("Joker");
		gesUsu.a�adirUsuario("Juan", 1100);
		System.out.println("Listar USUARIOS: ");
		gesUsu.listarUsuarios();
		EjemploHashMap.probandoHashMap();


		
		// System.out.println(this.obtenerUsuario("Joker").toString());

		if (joker.equals(usu)) {
			System.out.println("Son Iguales 2");
		} else {
			System.out.println("Son Diferentes 2");
		}

		gesUsu.modificar(u2, "Racso", 200);
		gesUsu.modificar("Racso", "RacsoII", 2);

		gesUsu.modificar(u3, "Namreg");
		gesUsu.modificar("Namreg", 888);
		gesUsu.eliminarUsuario("RacsoII");
		System.out.println("Listar USUARIOS: ");
		gesUsu.listarUsuarios();
		gesUsu.filtrarXEdad(888);
		gesUsu.modificar("Namreg", "German");
		gesUsu.modificar("German", 0);
		gesUsu.listarUsuarios();
		gesUsu.filtrarXEdad(20,800);
		
		gesUsu.eliminarTodos();
		System.out.println("Listar USUARIOS: ");
		gesUsu.listarUsuarios();
		
		//Caundo hay dos es un array bidimensional, es decir tendra 8 filas y 8 columnas cada uno
		int ajedrez[][]= new int[8][8];//64 elementos, 8*8
		ajedrez[1]=new int[3];
		ajedrez[2]=new int[300];
		System.out.println("LTama�o Ajedrez: " + ajedrez.length);
		System.out.println("LTama�o Ajedrez 0: " + ajedrez[0].length);
		System.out.println("LTama�o Ajedrez 1: " + ajedrez[1].length);
		System.out.println("LTama�o Ajedrez 2: " + ajedrez[2].length);
		System.out.println("LTama�o Ajedrez 3: " + ajedrez[3].length);
	*/
	}

}
