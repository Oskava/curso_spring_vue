
//La primera clase se tiene que llamar igual que el archivo, una clase sirve para definir propiedades o funciones que tienen objetos, una interfaz es la manera de comunicarse entre dos objetos o dos clases.  Los enum son las constantes de un metodo
public class IntroJava {
	
	//algo estatico es un metodo global, void es que no retorna nada
	public static void main(String[] args) {
		//System es clase, out es un objeto cuyo tipo es otra clase, println es un objeto, argumento es el valor que se envia al parámertro
		System.out.println("Bienvenidos a ICA");
		repetirMensaje(5);
	}
	//obliga a indicar que es lo que devuelve (void) y sus parámertro
	private static void repetirMensaje(int veces){
				String mensajeFun="Variable local a la funcion";
				for (int i=1; i<=veces; i++){
					String texto=mensajeFun + " " + i;
					System.out.println("<p>" + mensajeFun + " " + i + "</p>");
				}
				//System.out.println("<p>" + mensajeFun + "</p>");

				//Aqui no podriamos usar texto
			}
}