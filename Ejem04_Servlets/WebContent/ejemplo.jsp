<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejemplo JSP</title>
</head>
<body>
<h1>Ejemplo JSP</H1>
<!-- Se pone la exclamacion para indicar que se crea funcion
 --><%! String crearOL ( int v ){
	String ol ="<OL>";
	int i = v;
	while(i > 0){
		ol+= "<LI>Cuenta atr�s" + i + "/ 5 </LI>";
		i--;
	}
	return ol + "</OL>";
}
%>
<HR/>
<% 
Date d = new Date();
out.println(d.toString());
if (d.getSeconds() % 2 ==0){%>
	<p style="background-color: red">Prueba otra vez, hasta un segundo impar</p>
<%}else{ %>
	<p style="background-color: blue"></p>
<!-- se invoca a la funcion con %=
 -->	<%=crearOL(d.getSeconds())  %>
 		<%--Lo anterior es los mismo que esto:
 		<% out.println(crearOL(5)); --%>
<HR/>
<% } %>
</body>
</html>