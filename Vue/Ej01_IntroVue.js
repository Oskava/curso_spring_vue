Vue.component("adios", {
    "template": `<div>
    <h1> Adiosss </h1>`
});

Vue.component("arrepentimiento", {
    "template": `<div>
    <h1> Hola de nuevo</h1>
    <h2> Me arrepiento de despedirte</h2>`
})

new Vue({
    //le pasamo un objeto (el) y la # indica que cogemos un elemento por su ID
    "el": "#app-section",
    //le asignamos la plantilla html
    template: `<div><h2>Hola ICA</h2>
    <adios></adios>
    </div>`
});

new Vue({
    //le pasamo un objeto (el) y la # indica que cogemos un elemento por su ID
    "el": "#app-section2",
    //le asignamos la plantilla html
    template: `<div>
    <arrepentimiento></arrepentimiento>
    </div>`
});